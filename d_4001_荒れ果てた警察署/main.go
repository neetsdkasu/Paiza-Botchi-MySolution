package main

import "fmt"

func main() {

	var n1, n2 int
	fmt.Scan(&n1, &n2)

	n3 := (n1 + n2) % 10

	fmt.Println(n3)
}
