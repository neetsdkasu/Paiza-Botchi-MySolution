package main

import "fmt"

func main() {

	var n, w int
	fmt.Scan(&n, &w)

	ans := n - w

	if ans < 0 {
		fmt.Println("error")
	} else {
		fmt.Println(ans)
	}

}
