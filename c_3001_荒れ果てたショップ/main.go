package main

import "fmt"

func main() {

	var n, a, b int
	fmt.Scan(&n, &a, &b)

	fs := fmt.Sprint("%0", n, "d") + fmt.Sprintln()

	for i := a; i <= b; i++ {
		fmt.Printf(fs, i)
	}
}
