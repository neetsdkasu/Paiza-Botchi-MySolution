package main

import "fmt"

func main() {

	var h, w, n int
	fmt.Scan(&h, &w, &n)

	field := NewField(h, w)

	buildings := make([]*Building, 0, n)

	var maximum *Building = nil

	for i := 1; i <= n; i++ {
		building := &Building{}
		building.n = i
		fmt.Scan(&building.h, &building.w, &building.r, &building.c)
		buildings = append(buildings, building)
		if maximum == nil || building.Size() > maximum.Size() {
			maximum = building
		}
	}

	field.Set(maximum, 0, 0)

	fmt.Print(field)
}

type Row []int

type Field []Row

func NewRow(w int) Row {
	return make(Row, w)
}

func NewField(h, w int) Field {
	field := make(Field, h)
	for r := range field {
		field[r] = NewRow(w)
	}
	return field
}

func (field Field) H() int {
	return len(field)
}

func (field Field) W() int {
	return len(field[0])
}

func (field Field) String() string {
	ret := ""
	for _, row := range field {
		for i, a := range row {
			if i > 0 {
				ret += " "
			}
			ret += fmt.Sprint(a)
		}
		ret += fmt.Sprintln()
	}
	return ret
}

func (field Field) Set(building *Building, r, c int) bool {
	if building == nil {
		return false
	}
	if r < 0 || r+building.h > field.H() {
		return false
	}
	if c < 0 || c+building.w > field.W() {
		return false
	}
	for dr := 0; dr < building.h; dr++ {
		for dc := 0; dc < building.w; dc++ {
			if field[r+dr][c+dc] != 0 {
				return false
			}
		}
	}
	for dr := 0; dr < building.h; dr++ {
		for dc := 0; dc < building.w; dc++ {
			field[r+dr][c+dc] = building.n
		}
	}
	return true
}

type Building struct {
	n, h, w, r, c int
}

func (building *Building) Size() int {
	return building.h * building.w
}
