package main

import "fmt"

func main() {

	var n int
	fmt.Scan(&n)

	var ans Team

	for i := 1; i <= n; i++ {
		var team Team
		team.s = i
		fmt.Scan(&team)
		if team.t > ans.t {
			ans = team
		}
	}

	fmt.Println(ans)
}

type Team struct {
	s, t, W, D, L int
}

func (team *Team) Scan(state fmt.ScanState, verb rune) error {
	token, err := state.Token(false, nil)
	if err != nil {
		return err
	}
	for _, ch := range token {
		switch ch {
		case 'W':
			team.t += 2
			team.W++
		case 'D':
			team.t += 1
			team.D++
		case 'L':
			team.L++
		}
	}
	return nil
}

func (team Team) String() string {
	return fmt.Sprint(team.s, team.t, team.W, team.D, team.L)
}
