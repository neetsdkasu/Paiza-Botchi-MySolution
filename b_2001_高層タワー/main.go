package main

import (
	"fmt"
	"strings"
)

func main() {

	var n int
	fmt.Scan(&n)

	ans := ""

	for i := 0; i < n; i++ {
		var w string
		fmt.Scan(&w)

		runes := []rune(w)
		for e := len(w); e >= 0; e-- {
			head := string(runes[:e])
			tail := string(runes[e:])
			if strings.HasSuffix(ans, head) {
				ans += tail
				break
			}
		}
	}

	fmt.Println(ans)
}
