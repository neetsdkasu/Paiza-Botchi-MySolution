package main

import "fmt"

func main() {

	var n int
	fmt.Scan(&n)

	w_a := 0
	w_b := 0

	for i := 0; i < n; i++ {
		var a, b string
		fmt.Scan(&a, &b)
		ab := a + b
		switch ab {
		case "gc", "cp", "pg":
			w_a++
		case "cg", "pc", "gp":
			w_b++
		}
	}

	fmt.Println(w_a)
	fmt.Println(w_b)
}
