package main

import "fmt"

func main() {

	var h, w, n int
	fmt.Scan(&h, &w, &n)

	valid := true
	x, y := 0, 0

	for i := 0; i < n; i++ {
		var cmd string
		fmt.Scan(&cmd)
		switch cmd {
		case "U":
			y++
		case "D":
			y--
		case "L":
			x--
		case "R":
			x++
		}
		valid = valid && 0 <= y && y < h && 0 <= x && x < w
	}

	if valid {
		fmt.Println("valid")
	} else {
		fmt.Println("invalid")
	}

}
