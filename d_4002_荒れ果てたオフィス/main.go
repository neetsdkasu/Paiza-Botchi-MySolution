package main

import "fmt"

func main() {

	var n int
	fmt.Scan(&n)

	for i := 0; i < n; i++ {
		var s string
		var e int
		fmt.Scan(&s, &e)
		if e == 3 {
			fmt.Println(s)
		}
	}

}
