package main

import "fmt"

func main() {

	var n int
	fmt.Scan(&n)

	ans := 0

	for i := 0; i < n; i++ {
		var d int
		fmt.Scan(&d)
		if d > 5 {
			ans++
		}
	}

	fmt.Println(ans)
}
