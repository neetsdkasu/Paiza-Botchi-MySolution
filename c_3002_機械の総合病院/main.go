package main

import (
	"fmt"
	"unicode"
)

func main() {

	var t string
	fmt.Scan(&t)

	if isValid(t) {
		fmt.Println("Valid")
	} else {
		fmt.Println("Invalid")
	}
}

func isValid(s string) bool {

	if len(s) < 6 {
		return false
	}

	if isAll(s, unicode.IsDigit) {
		return false
	}

	if isAll(s, unicode.IsLetter) {
		return false
	}

	runes := []rune(s)
	for i := 2; i < len(runes); i++ {
		if runes[i-2] == runes[i] && runes[i-1] == runes[i] {
			return false
		}
	}

	return true
}

func isAll(s string, check func(rune) bool) bool {
	for _, r := range []rune(s) {
		if !check(r) {
			return false
		}
	}
	return true
}
