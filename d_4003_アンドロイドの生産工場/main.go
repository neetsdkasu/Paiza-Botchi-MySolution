package main

import "fmt"

func main() {

	var s string
	fmt.Scan(&s)

	ans := []rune{}

	for i, r := range []rune(s) {
		if i%2 == 0 {
			ans = append(ans, r)
		}
	}
	fmt.Println(string(ans))
}
